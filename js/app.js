/**
 * Created by mrb on 3/6/18.
 */

var reg = new RegExp(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/ , "i");
var books = [
    {
        id : '1',
        title : 'Css and Html web design' ,
        category : 'web',
        author : 'richard',
        img : 'https://about.canva.com/wp-content/uploads/sites/3/2015/01/business_bookcover.png'
    } ,

    {
        id : '2' ,
        title : 'Fred' ,
        category : 'Novel',
        author : 'Lowinsky',
        img : 'https://about.canva.com/wp-content/uploads/sites/3/2015/01/children_bookcover.png'
    }
];


var elementContext = {
    email:{},
    name:{},
    lastName:{},
    bookCount : {},

    bookGrid: {},
    bookTitle:{},
    bookCategory:{},
    bookAuthor : {},
    bookUrl : {},

    bookCardWrap : {},

    thumbnailCover : {},
    thumbnailTitle : {},
    thumbnailCategory : {},
    thumbnailAuthor : {},
    thumbnailDisc: {}


};



/**
 * html tags that should insert in page
 * @type {string}
 */

var bookCardHtml = '<li class="book-card-list">' +
    '<div class="book-card">' +
    '<div class="book-cover">' +
    '<img id="bookCoverImage" src="" alt="">' +
    '<div class="book-action">' +
    '<div id="bookId" style="display: none"></div>'+
    '<span class="edit" role="button" onclick="edit(event)">' +
    '<i class="material-icons" >mode_edit</i>' +
    '</span>' +
    '<span class="show-detail" role="button" onclick="moreDetail(event)">' +
    '<i class="material-icons">zoom_in</i>' +
    '</span>' +
    '<span class="delete" role="button" onclick="onDelteShowDialog(event)">' +
    '<i class="material-icons">delete</i>'+
    '</span>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</li>';

var validationHtml = {
    title : '<div class="unvalid-input">لطفا نام کتاب را وارد کنید!</div>' ,
    category : '<div class="unvalid-input">لطفا دسته بندی را وارد کنید!</div>',
    author : '<div class="unvalid-input">لطفا نام ناشر را وارد کنید!</div>',
    imgUrl : '<div class="unvalid-input">لطفا لینک عکس را وارد کنید!</div>',
    titleCharLimit : '<div class="unvalid-input">تعداد حروف بیش از حد مجاز</div>',
    categoryCharLimit : '<div class="unvalid-input">تعداد حروف بیش از حد مجاز</div>',
    authorCharLimit : '<div class="unvalid-input">تعداد حروف بیش از حد مجاز</div>',
    urlValidtion : '<div class="unvalid-input">آدرس عکس معتبر نیست</div>'
}

/*----------------------------------------------------------------------------*/




function changeTab(target) {

    var targetContent = document.getElementById(target);
    var hasShow = targetContent.classList.contains('show');
    if(!hasShow){
        var currentOpenTabContent = targetContent.parentElement.getElementsByClassName('show');
        currentOpenTabContent[0].classList.toggle('show');
        targetContent.classList.toggle('show');

        var currentActiveTab = document.getElementById('addBook');
        var currentPassiveTab = document.getElementById('bookList');

        currentActiveTab.classList.toggle('active');
        currentPassiveTab.classList.toggle('active');
        if(currentPassiveTab.classList.contains('active')){
            currentActiveTab.innerText = 'افزودن';
        }


    }


}

(function(){
    elementContext.email = document.getElementById('email');
    elementContext.name= document.getElementById('name');
    elementContext.lastName = document.getElementById('lastName');
    elementContext.bookGrid = document.getElementById('bookGrid');
    elementContext.bookTitle = document.getElementById('bookTitle');
    elementContext.bookCategory = document.getElementById('bookCategory');
    elementContext.bookAuthor = document.getElementById('bookAuthor');
    elementContext.bookUrl = document.getElementById('bookUrl');
    elementContext.bookCount = document.getElementById('bookCount');

    elementContext.thumbnailCover = document.getElementById('tumbnailCoverImage');
    elementContext.thumbnailTitle = document.getElementById('bookThumbnailTitle');
    elementContext.thumbnailCategory = document.getElementById('bookThumbnailCategory');
    elementContext.thumbnailAuthor = document.getElementById('bookThumbnailAuthor');
    elementContext.thumbnailDisc = document.getElementById('bookThumbnailDiscription');
    var user = localStorage.getItem('user');
    user = JSON.parse(user);
    elementContext.name.innerText =  'نام : ' + user[0].name;
    elementContext.lastName.innerText =  'نام خانوادگی : ' + user[0].lastName;
    elementContext.email.innerText =  'ایمیل : ' + user[0].email;
    elementContext.bookCount.innerText = ' تعداد کتاب ها :' + books.length;

    fillBookList();

    var url = "http://mediad.publicbroadcasting.net/p/wamc/files/styles/x_large/public/201708/alexandriamarzanolesnivich-thefactofabody.jpg";

    console.log(url.match(reg))


})();


function addBookToList(book) {
    books.push(book);
    elementContext.bookGrid.insertAdjacentHTML('afterbegin' , bookCardHtml);
    document.getElementById('bookCoverImage').src = book.img;
    document.getElementById('bookId').innerText = book.id;
    elementContext.bookCount.innerText = ' تعداد کتاب ها :' + books.length;
}

function applyEditedBook(book) {
    books.push(book);
    fillBookList();
}

/**
 * draw books that are in the books array into the page
 * it is almost like re rendering the page
 */

function fillBookList() {

    var editedCard = document.getElementsByClassName('book-card-list');
    var k = 0;
    var length = editedCard.length;

    while (k < length ){
        editedCard[0].remove();
        k++;
    }

    for(var i = 0 ; i< books.length ; i++){
        elementContext.bookGrid.insertAdjacentHTML('afterbegin' , bookCardHtml);
        document.getElementById('bookCoverImage').src = books[i].img;
        document.getElementById('bookId').innerText = books[i].id;
    }
    elementContext.bookCount.innerText = ' تعداد کتاب ها :' + books.length;
}

/**
 * create new book if it was in edit mode or in new book mode
 * @param target
 */

function onCreateBookFormSubmit(target) {
    if (validateFrom()){
        var bId = books.length;
        var edit = false;
        var bookIdEdit;
        for (var i = 0 ; i < bId ; i++){
            if (books[i].title === elementContext.bookTitle.value || books[i].author === elementContext.bookTitle.value ||
                books[i].category === elementContext.bookCategory.value || books[i].img === elementContext.bookUrl.value){
                edit = true;
                bookIdEdit = books[i].id;
            }
        }

        if (edit){
           books = books.filter(function (book) {
               return book.id !== bookIdEdit;
           });

            createBook(bookIdEdit , true);

        }
        else {
           createBook(bId +1 + '' , false);
        }

        resetInput();

      changeTab(target);
    }
}

/**
 * reset all input field
 */
function resetInput() {
    elementContext.bookTitle.value = '';
    elementContext.bookCategory.value = '';
    elementContext.bookAuthor.value = '';
    elementContext.bookUrl.value = '';
}

function createBook(id , edit) {
    var book = {
        id : id ,
        title : elementContext.bookTitle.value ,
        category : elementContext.bookCategory.value ,
        author : elementContext.bookAuthor.value,
        img : elementContext.bookUrl.value
    };
    if (edit){
        applyEditedBook(book)
    }
    else {
        addBookToList(book);
    }

}

function validateFrom() {
    var unvalidInput = document.getElementsByClassName('unvalid-input');
    var length = unvalidInput.length;
    var k = 0;
    var flag = true;
    while (k < length ){
        unvalidInput[0].remove();
        k++;
    }
    if (elementContext.bookTitle.value.trim() === ''){
        elementContext.bookTitle.parentElement.insertAdjacentHTML('beforeend' , validationHtml.title);
        flag = false;
    }
    if (elementContext.bookCategory.value.trim() === ''){
        elementContext.bookCategory.parentElement.insertAdjacentHTML('beforeend' , validationHtml.category);
        flag = false;
    }
    if (elementContext.bookAuthor.value.trim() === ''){
        elementContext.bookAuthor.parentElement.insertAdjacentHTML('beforeend' , validationHtml.author);
        flag = false;
    }
    if (elementContext.bookUrl.value.trim() === ''){
        elementContext.bookUrl.parentElement.insertAdjacentHTML('beforeend' , validationHtml.imgUrl);

        flag = false;
    }

    if (elementContext.bookTitle.value.length > 20){
        elementContext.bookTitle.parentElement.insertAdjacentHTML('beforeend' , validationHtml.titleCharLimit);
        flag = false;
    }
    if (elementContext.bookAuthor.value.length > 20){
        elementContext.bookAuthor.parentElement.insertAdjacentHTML('beforeend' , validationHtml.authorCharLimit);
        flag = false;
    }
    if (elementContext.bookCategory.value.length > 20){
        elementContext.bookCategory.parentElement.insertAdjacentHTML('beforeend' , validationHtml.categoryCharLimit);
        flag = false;
    }

    if (elementContext.bookUrl.value.match(reg) === null && elementContext.bookUrl.value.trim() !== ''){
        elementContext.bookUrl.parentElement.insertAdjacentHTML('beforeend' , validationHtml.urlValidtion);
        flag = false;
    }


    return flag;
}

function edit(e) {
    var editTab = document.getElementById('addBook');
    editTab.innerText = 'ویرایش';
    changeTab('content2');

    var id = 0;

    id = extractElementId(e);

    var result = books.filter(function (book) {

        if (book.id === id){
            return book;
        }
    })
    elementContext.bookTitle.value = result[0].title;
    elementContext.bookCategory.value = result[0].category;
    elementContext.bookAuthor.value = result[0].author;
    elementContext.bookUrl.value = result[0].img;

}

function moreDetail(event) {

    var thumb = document.getElementById('showBookDetails');
    thumb.style.display = 'block';

    var id = extractElementId(event);

    window.onclick = function (event ) {
        if (event.target == thumb){
            thumb.style.display = 'none';
        }
    }

    var result = books.filter(function (book) {
        return book.id === id;
    })
    console.log(result)
    elementContext.thumbnailCover.src = result[0].img;
    elementContext.thumbnailTitle.textContent = 'اسم کتاب : ' + result[0].title;
    elementContext.thumbnailCategory.textContent = 'دسته بندی : ' + result[0].category;
    elementContext.thumbnailAuthor.textContent = 'ناشر : ' + result[0].author;



}


var deleteEvent ;
function onDelteShowDialog(e) {
    deleteEvent = e;
    var dialog = document.getElementById('deleteDialogBox');
    
    dialog.style.display = 'block';
    window.onclick = function (event ) {
        if (event.target == dialog){
            dialog.style.display = 'none';
        }
    }

}

/**
 * delete the proper book when you accept to delete it
 */
function deleteBookOnSubmit() {

    var id = 0;
    id = extractElementId(deleteEvent);


    books = books.filter(function (book) {
        return book.id !== id;
    });

    fillBookList();
    var dialog = document.getElementById('deleteDialogBox');
    dialog.style.display = 'none';

}


/**
 * close dialog when click on refuse button
 */
function closeOnRefuse() {
    var dialog = document.getElementById('deleteDialogBox');
    dialog.style.display = 'none';
}

function closeLogOutDialog() {
    var dialog = document.getElementById('logOutDialog');
    dialog.style.display = 'none';
}


/**
 * get id of element which is clicked
 * @param event target of mouse that is clicked on
 * @returns {string}
 */
function extractElementId(event) {

    if (event.target.nodeName === 'I'){
       return  event.target.parentNode.parentNode.firstChild.textContent;
    }
    else {
        return  event.target.parentNode.firstChild.textContent;
    }
}


function exit() {

    localStorage.clear();
    window.location.href = 'login.html';
}


function onExitDialogShow() {
    var dialog = document.getElementById('logOutDialog');
    dialog.style.display = 'block';

    window.onclick = function (event ) {
        if (event.target == dialog){
            dialog.style.display = 'none';
        }
    }
}








